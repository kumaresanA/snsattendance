import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { AuthProvider } from '../../providers/auth/auth'
import { AlertController } from 'ionic-angular';
import { LoadingController,Loading } from 'ionic-angular';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  username;
  password;
  college;
  collegelogo;
   loading:Loading;
  constructor(public navCtrl: NavController,private auth:AuthProvider,public alertCtrl: AlertController,public loadingCtrl: LoadingController) {

  }
  mypage(college){
  this.college=college;
  if (college=='tech') {
    this.collegelogo='./assets/img/snsct.png'
  } else if(college=='eng') {
    this.collegelogo='./assets/img/snsce.png'
  }
  else{
this.collegelogo='./assets/img/arts.jpeg'
  }
  }
login(){
  this.presentLoading();
  this.auth.login().subscribe(result=>{
   console.log(result.json());
   if (result.json().username==this.username&&result.json().password==this.password) {
     console.log("success")
     this.loading.dismiss()
   } else {
     this.showPrompt();
     this.loading.dismiss()
     console.log("False")
   }
  },
  error=>{

  })

}
showPrompt() {
    let prompt = this.alertCtrl.create({
      title: 'failed',
      message: "invalid credentials",
            buttons: [
      
        {
          text: 'ok',
          handler: data => {
            console.log('Saved clicked');
          }
        }
      ]
    });
    prompt.present();
  }
presentLoading() {
    this.loading = this.loadingCtrl.create({
      content: "Please wait...",
    });
    this.loading.present();
  }
}
